var getOsobe = () => Pozivi.osobljeSale()
    .then(osobe => {
       
        const tbody = document.getElementById("osobeBody");
        tbody.innerHTML = "";
        for(const osoba of osobe) {
            const row = document.createElement("tr");
            for(const key in osoba) {
                const td = document.createElement("td");
                td.innerHTML = osoba[key];
                row.appendChild(td);
            }
            tbody.appendChild(row);
        }
    });

setTimeout(getOsobe, 50);

setInterval(getOsobe, 30000);
