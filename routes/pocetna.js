
const data = {
    images: {
        "1": "",
        "2": "",
        "3": "",
        "4": "",
        "5": "",
        "6": "",
        "7": "",
        "8": "",
        "9": "",
        "10": ""
    },
    page: -1
};
// ucitajSlike(data);
setTimeout(() => ucitajSlike(data), 0);


async function ucitajSlike(data) {
    try {
        data.page++;
        console.log(data);

        try {
            if (data.page < 3) {
                document.getElementById("s").disabled = false;
                // data.page++;
            }
            if (data.page >= 1)
                document.getElementById("p").disabled = false;
            else
                document.getElementById("p").disabled = true;

            if (data.page == 3) {
                document.getElementById("s").disabled = true;
            }

            let x = document.getElementById("sakriti1");
            let y = document.getElementById("sakriti2");
            if (document.getElementById("s").disabled === true) {

                x.style.display = "none";
                y.style.display = "none";
                //ucitajSliku10();

            }
            else {
                x.style.display = "block";
                y.style.display = "block";
            }
        } catch (e) {
            console.log("ERROR");
        }
        const page = data.page;
        var images = document.querySelectorAll('img');

        let img1;
        let img2;
        let img3;
       
        let imgs = await Pozivi.citanjeSlika(page);
        img1 = imgs.img1;
        img2 = imgs.img2;
        img3 = imgs.img3;


        if (img1 && !img1.ok) {
            throw new Error('Network response was not ok.');
        }
        const myBlob1 = img1 ? await img1.blob() : null;
        const myBlob2 = img2 ? await img2.blob() : null;
        const myBlob3 = img3 ? await img3.blob() : null;

        const objectURL1 = myBlob1 ? URL.createObjectURL(myBlob1) : data.images[page * 3 + 1];
        const objectURL2 = myBlob2 ? URL.createObjectURL(myBlob2) : data.images[page * 3 + 2];
        const objectURL3 = myBlob3 ? URL.createObjectURL(myBlob3) : data.images[page * 3 + 3];

        console.log(objectURL1)
        images[1].src = objectURL1;
        images[2].src = objectURL2 ? objectURL2 : "";
        images[3].src = objectURL3 ? objectURL3 : "";

        images[1].className = "slika";
        images[2].className = "slika";
        images[3].className = "slika";
        if (!data.images[page * 3 + 1])
            data.images[page * 3 + 1] = objectURL1;

        if (!data.images[page * 3 + 2])
            data.images[page * 3 + 2] = objectURL2;

        if (!data.images[page * 3 + 3])
            data.images[page * 3 + 3] = objectURL3;



    } catch (error) {
        console.log('There has been a problem with your fetch operation: ', error.message);
    }


}

async function prethodna(data) {
    if (data.page > 0) {

        data.page -= 2;
        await ucitajSlike(data);

    }
    else {
        document.getElementById("p").disabled = true;

    }
}

setTimeout(() => Pozivi.posjete().then((data) => {
    const posjete = document.getElementById("posjete");
    posjete.innerHTML = `Posjetilaca: ${data.posjetilaca}<br>Chrome: ${data.chrome}<br>Firefox: ${data.firefox}`;
}), 10);