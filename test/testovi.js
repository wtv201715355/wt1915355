const assert = require('assert');
const chai = require("chai");
const chaiHttp = require("chai-http");
const sinon = require("sinon");
const MockBrowser = require('mock-browser').mocks.MockBrowser;
const fetch = require("node-fetch");
const mock = new MockBrowser();
global.document = mock.getDocument();
let div = document.createElement('div');
var fetchMock = require('fetch-mock');


chai.use(chaiHttp);
let should = chai.should();
const app = require("../index.js");
//const baza=require("../db.js"); povremeno ubaciti.. i promjeniti timeout
//test broj 1
describe('Provjera da li postoje svi dokumenti u html formatu', function (done) {
    let stranice = ["pocetna", "sale", "unos", "rezervacija", "osobe"];
    for (str in stranice) {
        it(`dohvata ${stranice[str]}`, function (done) {
            chai.request(app).get(`/${stranice[str]}`).end(function (err, res) {
                assert.equal(err, null, "Greska, odgovor od servera nije stigao!");
                res.should.have.header('Content-Type', /text\/html.+/);
                done();
            });
        });
    }
});
const osobljePodaci2 = [
    {
        id: 11,
        ime: "Neko",
        prezime: "Nekić",
        uloga: "profesor"
    }, {
        id: 22,
        ime: "Drugi",
        prezime: "Neko",
        uloga: "Asistent"
    },
    {
        id: 33,
        ime: "Test",
        prezime: "Test",
        uloga: "Asistent"
    }];
describe('Umetanje nove osobe u bazu', async () => {
    var redovi = 0;
    before(async function () {
        await baza.sequelize.sync({ force: true });
        const { count, rows } = await baza.Osoblje.findAndCountAll({});
        redovi = count;
    });
    it('Mora biti novi element bazi', async (done) => {
        // await baza.sequelize.sync({force: true});

        await baza.Osoblje.create(osobljePodaci2[1]);
        const { count1, rows1 } = await baza.Osoblje.findAndCountAll({});
        assert.equal(redovi + 1, count1);
        done();
    });

});
/*
describe('Dobivanje spiska svih sala', () => {
    var redovi=0;
    before(async function() {
        await baza.sequelize.sync({force: true});
        const { count, rows } = await baza.Sala.findAndCountAll({});
        redovi=count;

    });
    it('Mora biti vise od 0 redova u tabeli sala', (done) => {

            assert.notEqual(redovi, 0);
            done();

    });

    
});*/

//test broj 2
describe('Provjera sala', function () {
    it('vraća json sale', function (done) {

        chai.request(app).get('/spisaksala').end(function (err, res) {
            res.should.have.status(200);
            res.should.have.header("Content-Type", /application\/json/);
            done();
        });
    });
});
//test broj 3
describe('Provjerava osoblje', function () {
    it('vraća json osoblje', function (done) {
        chai.request(app).get('/osoblje').end(function (err, res) {
            res.should.have.status(200);
            res.should.have.header("Content-Type", /application\/json/);
            done();
        });
    });
});

//test broj 4
describe("Testiranje ajax poziva za spisak sala", function () {
    //provjeriti da li je exportovan modul pozivi.js

    beforeEach(function () {

    });
    afterEach(function () {
        fetchMock.restore();
    });
    it('salje AJAX zahtjev', function (done) {
        setTimeout(async () => {
            fetchMock.mock('http://127.0.0.1:8080/spisaksala', 200);
            const res = await fetch('http://127.0.0.1:8080/spisaksala');
            assert(res.ok);
            done();
        }, 5000);

    });

});
describe("Testiranje ajax poziva za spisak osoblje", function () {
    //provjeriti da li je exportovan modul pozivi.js

    beforeEach(function () {

    });
    afterEach(function () {
        fetchMock.restore();
    });
    it('salje AJAX zahtjev', function (done) {
        setTimeout(async () => {
            fetchMock.mock('http://127.0.0.1:8080/osoblje', 200);
            const res = await fetch('http://127.0.0.1:8080/osoblje');
            assert(res.ok);
            done();
        }, 8000);

    });

});

describe('Šalje rezervaciju', function () {
    it('vraća json osoblje', function (done) {
        chai.request(app).post('/rezervacija')
        .send({"sala":1, "periodicnost":true, "pocetak":"12:00", "kraj":"13:00", "osoba":1, "semestar":1})
        end(function (err, res) {
            res.should.have.status(200);
            res.should.have.header("Content-Type", /application\/json/);//šalje json
            done();
        });
    });
});

describe('Šalje rezervaciju periodičnu', function () {
    it('vraća json osoblje', function (done) {
        chai.request(app).post('/rezervacija')
        .send({"sala":1, "periodicnost":true, "pocetak":"12:00", "kraj":"13:00", "osoba":1, "semestar":1})
        end(function (err, res) {
            res.should.have.status(200);
            res.should.have.header("Content-Type", /application\/json/);//šalje json
            done();
        });
    });
});


describe('Šalje rezervaciju neperiodičnu', function () {
    it('vraća json osoblje', function (done) {
        chai.request(app).post('/rezervacija')
        .send({"sala":1, "periodicnost":false, "pocetak":"12:00", "kraj":"13:00", "osoba":1, "semestar":null, "datum":"1.1.2020"})
        end(function (err, res) {
            res.should.have.status(200);
            res.should.have.header("Content-Type", /application\/json/);//šalje json
            done();
        });
    });
});
