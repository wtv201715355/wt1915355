const db = {};
const Sequelize = require("sequelize");

const sequelize = new Sequelize("DBWT19", "root", "root", {
    host: "localhost",
    dialect: "mysql",
    define: { timestamps: false }
});
db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.Termin = sequelize.import('./modeli/Termin.js');
db.Rezervacija = sequelize.import('./modeli/Rezervacija.js');
db.Sala = sequelize.import('./modeli/Sala.js');
db.Osoblje = sequelize.import('./modeli/Osoblje.js')


console.log(`Kreirana baza ${sequelize}`);


//relacije
db.Osoblje.hasMany(db.Rezervacija, { foreignKey: 'osoba' });
db.Rezervacija.belongsTo(db.Osoblje, { foreignKey: 'osoba' });


db.Termin.hasOne(db.Rezervacija, { foreignKey: 'termin' });
db.Rezervacija.belongsTo(db.Termin, { foreignKey: 'termin' });

db.Sala.hasMany(db.Rezervacija, { foreignKey: 'sala' });
db.Rezervacija.belongsTo(db.Sala, { foreignKey: 'sala' });

db.Osoblje.hasOne(db.Sala, { foreignKey: 'zaduzenaOsoba' });
db.Sala.belongsTo(db.Osoblje, { foreignKey: 'zaduzenaOsoba' });


const osobljePodaci = [
    {
        id: 1,
        ime: "Neko",
        prezime: "Nekić",
        uloga: "profesor"
    }, {
        id: 2,
        ime: "Drugi",
        prezime: "Neko",
        uloga: "Asistent"
    },
    {
        id: 3,
        ime: "Test",
        prezime: "Test",
        uloga: "Asistent"
    }];
const salaPodaci = [{
    id: 1,
    naziv: "1-11",
    zaduzenaOsoba: 1
}, {
    id: 2,
    naziv: "1-15",
    zaduzenaOsoba: 2
}];
const terminiPodaci = [{
    id: 1,
    redovni: false,
    dan: null,
    datum: "01.01.2020",
    semestar: null,
    pocetak: "12:00",
    kraj: "13:00"
}, {
    id: 2,
    redovni: true,
    dan: 0,
    datum: null,
    semestar: 1,
    pocetak: "13:00",
    kraj: "14:00"
},

{
    id: 3,
    redovni: true,
    dan: null,
    datum: "21.01.2020",
    semestar: 1,
    pocetak: "20:00",
    kraj: "22:00"
},
    {
        id: 4,
        redovni: true,
        dan: 1,
        datum: null,
        semestar: 1,
        pocetak: "20:00",
        kraj: "22:00"
    }];
const rezervacijaPodaci = [{
    id: 1,
    termin: 1,
    sala: 1,
    osoba: 1
},
{
    id: 2,
    termin: 2,
    sala: 1,
    osoba: 3
},

{
    id: 3,
    termin: 3,
    sala: 1,
    osoba: 3
},
    {
        id: 4,
        termin: 4,
        sala: 2,
        osoba: 1
    }];
const osobljePodaci2 = [
    {
        id: 11,
        ime: "Neko",
        prezime: "Nekić",
        uloga: "profesor"
    }, {
        id: 22,
        ime: "Drugi",
        prezime: "Neko",
        uloga: "Asistent"
    },
    {
        id: 33,
        ime: "Test",
        prezime: "Test",
        uloga: "Asistent"
    }];
db.sequelize.sync({ force: true })
    .then(function () {
        // db.Osoblje.bulkCreate(osobljePodaci);
        // db.Sala.bulkCreate(salaPodaci);
        // db.Termin.bulkCreate(terminiPodaci);
        // db.Rezervacija.bulkCreate(rezervacijaPodaci);
        // //db.Osoblje.create(osobljePodaci2[0]);

        db.Osoblje.bulkCreate(osobljePodaci).then(() => {
            db.Sala.bulkCreate(salaPodaci).then(() => {
                db.Termin.bulkCreate(terminiPodaci).then(() => {
                    db.Rezervacija.bulkCreate(rezervacijaPodaci);
                });
            });
        });
    });
// db.Termin.create()

module.exports = db;

