
const express = require('express');
const path = require('path');
const app = express();
const port = 8080;
const fs = require('fs');
const bodyParser = require('body-parser');
const http = require('http');
const requestIp = require('request-ip');
var useragent_parser = require('./node_modules/useragent_parser_browser');
var chrome = 0;
var ff = 0;
var ipPosjetilaca = [];
const hostname = '127.0.0.1';
const db = require("./db.js");

const { Op } = require("sequelize");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));
app.use((req, res, next) => {
    console.log(`${req.method} ${req.url}`);
    const clientIp = requestIp.getClientIp(req);
    if (!ipPosjetilaca.includes(clientIp)) {
        ipPosjetilaca.push(clientIp);
    }
    //console.log( req.body);
    next();
});

app.get("/podaci", function (req, res) {

    db.Rezervacija.findAll({ include: [db.Osoblje, db.Termin, db.Sala] }).then(rezervacije => {
        // try {
        //     // rezervacije[0].getOsoba().then(osoba => console.log(osoba));
        //     console.log(rezervacije);
        // }
        // catch (e) {
        //     console.error(e);
        // }
        // console.log(rezervacije[0].getOsoba());
        res.end(JSON.stringify(rezervacije));
    });
});
/*
app.post("/podaci", function (req, res) {
    console.log(req.body);
    var obj = JSON.parse(fs.readFileSync('./server/zauzeca.json', 'utf8'));
    let postoji = false;
    for (const zauzece of obj.periodicna) {
        // if(...) { // provjera preklapanja sa postojecim zauzecima
        //    postoji = true;
        //    break;
        // }
    }
    for (const zauzece of obj.vanredna) {
        // if(...) { // provjera preklapanja sa postojecim zauzecima
        //    postoji = true;
        //    break;
        // }
    }
    if (!postoji) {
        // provjera je li periodicno ili vanredno...
        obj.vanredna.push(req.data);
        fs.writeFileSync('zauzeca.json', JSON.stringify(obj));
        res.status(200);
        res.send();
    } else {
        res.status(400);
        res.send("Greska: Sala je zauzeta u trazenom terminu!");
    }
    // res.end(obj);
});*/
app.get("/rezervacija", function (req, res) {

    res.sendFile('public/html/rezervacija.html', { root: __dirname });
});

app.get("/", function (req, res) {
    if (useragent_parser.parse(req.headers["user-agent"]).family == 'Chrome') {
        chrome++;
    }
    if (useragent_parser.parse(req.headers["user-agent"]).family == 'Firefox') {
        ff++;
    }
    console.log(`chrome: ${chrome} firefox: ${ff}`);
    res.sendFile('public/html/pocetna.html', { root: __dirname });
});
app.get("/sale", function (req, res) {

    res.sendFile('public/html/sale.html', { root: __dirname });
});
app.get("/rezervacija", function (req, res) {
    res.sendFile('public/html/rezervacija.html', { root: __dirname });
});
app.get("/unos", function (req, res) {

    res.sendFile('public/html/unos.html', { root: __dirname });
});
app.get("/posjete", function (req, res) {
    const obj = {
        posjetilaca: ipPosjetilaca.length,
        chrome: chrome,
        firefox: ff
    };
    res.json(obj);
});
app.get("/pocetna", function (req, res) {
    if (useragent_parser.parse(req.headers["user-agent"]).family == 'Chrome') {
        chrome++;
    }
    if (useragent_parser.parse(req.headers["user-agent"]).family == 'Firefox') {
        ff++;
    }
    console.log(`chrome: ${chrome} firefox: ${ff}`);
    res.sendFile('public/html/pocetna.html', { root: __dirname });
});
app.get("/publicimgClassroom-131-Side-View.jpg", function (req, res) {
    res.sendFile("public/img/Classroom-131-Side-View.jpg", { root: __dirname });
});

app.get("/img/:name", function (req, res) {
    try {
        if (name < 11) {
            res.sendFile(`public/img/${name}.jpg`, { root: __dirname });
        } else {

        }

    } catch (error) {

    }

});
app.get("/routes/rezervacija.js", function (req, res) {
    res.sendFile('routes/rezervacija.js', { root: __dirname });
});
app.get("/routes/sale.js", function (req, res) {
    res.sendFile('routes/rezervacija.js', { root: __dirname });
});
app.get("/osoblje", async function (req, res) {
    const osoblje = await db.Osoblje.findAll();
    res.setHeader("Content-Type", "application/json");
    res.send(JSON.stringify(osoblje));
});

app.get("/osobe", function (req, res) {
    res.sendFile('public/html/osoblje.html', { root: __dirname });

});
// app.get("/zauzetoosoblje", async function (req, res) {
//     db.Rezervacija.findAll({ include: [db.Osoblje, db.Termin, db.Sala] }).then(rezervacije => {

//         console.warn(rezervacije);
//         // const o = rezervacije.flatMap(r => r.Osoblje);
//         const o = Array.call([].flatMap, rezervacije, r => r.Osoblje);

//         console.error(o);

//         try {
//             // rezervacije[0].getOsoba().then(osoba => console.log(osoba));
//             // console.log(rezervacije);
//             db.Osoblje.findAll().then(async osoblje => {
//                 const osobe = [];
//                 // for(osoba of osoblje) {
//                 //     for(rezervacija of rezervacije) {
//                 //         if(rezervacija.osoba === osoba.id) {

//                 //         }
//                 //     }
//                 // }

//                 for(osoba of osoblje) {
//                     if(!o.includes(osoba)) {
//                         osobe.push({ ime: osoba.ime, prezime: osoba.prezime, uloga: osoba.uloga, sala: "U kancelariji" });
//                     } else {
//                         const date = new Date();
//                         const datum = date.getDate();
//                         const sati = date.getHours();
//                         const minute = date.getMinutes();

//                         const rezervacija = await db.Rezervacija.findOne({
//                             include: [db.Termin, db.Sala],
//                             where: {
//                                 osoba: osoba.id,
//                                 datum,
//                                 pocetak: {
//                                     [Op.lt]: `${sati}:${minute}`
//                                 },
//                                 kraj: {
//                                     [Op.gt]: `${sati}:${minute}`
//                                 }
//                             }
//                         });
//                         if(rezervacija)
//                             osobe.push({ ime: osoba.ime, prezime: osoba.prezime, uloga: osoba.uloga, sala: rezervacija.Sala.naziv });
//                         else 
//                             osobe.push({ ime: osoba.ime, prezime: osoba.prezime, uloga: osoba.uloga, sala: "U kancelariji" });
//                     }
//                 }

//                 res.send(JSON.stringify(osobe));
//             });
//         }
//         catch (e) {
//             console.error(e);
//         }
//         // console.log(rezervacije[0].getOsoba());
//         res.end(JSON.stringify(rezervacije));
//     });

// });



app.get("/zauzetoosoblje", async function (req, res) {
    const rezervacije = await db.Rezervacija.findAll({ include: [db.Osoblje, db.Termin, db.Sala] });

    // const o = [].flatMap.call(rezervacije, [r => r.Osoblje]);
    const o = [...rezervacije.map(r => r.Osoblje)];

    // console.error(o);

    const osoblje = await db.Osoblje.findAll()
    const osobe = [];

    for (osoba of osoblje) {
        // if (!o.includes(osoba)) {
        //     osobe.push({ ime: osoba.ime, prezime: osoba.prezime, uloga: osoba.uloga, sala: "U kancelariji" });
        // } else {
        if (!o.map(os => os.id).includes(osoba.id)) {
            osobe.push({ ime: osoba.ime, prezime: osoba.prezime, uloga: osoba.uloga, sala: "U kancelariji" });
        } else {
            const date = new Date();
            const dan = date.getDate();
            const mjesec = date.getMonth() + 1;
            const godina = date.getFullYear();
            const sati = date.getHours();
            const minute = date.getMinutes();

            let semestar = 0;
            const zimski = [10, 11, 1,0, 2];
            const ljetni = [3, 4, 5, 6];

            if(zimski.includes(mjesec))
                semestar = 1;
            else if(ljetni.includes(mjesec))
                semestar = 2;

            const danUSedmici = (date.getDay()+6)%7;//ponedjeljak mora biti 0, a u javascriptu je 1, pa sam ovo sad nastelio

            const rezervacija = await db.Rezervacija.findOne({
                include: [{
                    model: db.Termin,
                    where: {
                        datum: `${dan}.${mjesec < 10 ? '0' : ''}${mjesec}.${godina}`,
                        pocetak: {
                            [Op.lt]: `${sati}:${minute}`
                        },
                        kraj: {
                            [Op.gt]: `${sati}:${minute}`
                        }
                    }
                }, db.Sala],
                where: {
                    osoba: osoba.id
                }
            });

            const rezervacija2 = await db.Rezervacija.findOne({
                include: [{
                    model: db.Termin,
                    where: {
                        dan: danUSedmici,
                        semestar,
                        pocetak: {
                            [Op.lt]: `${sati}:${minute}`
                        },
                        kraj: {
                            [Op.gt]: `${sati}:${minute}`
                        }
                    }
                }, db.Sala],
                where: {
                    osoba: osoba.id
                }
            });

            if (rezervacija)
                osobe.push({ ime: osoba.ime, prezime: osoba.prezime, uloga: osoba.uloga, sala: rezervacija.Sala.naziv });
            else if (rezervacija2)
                osobe.push({ ime: osoba.ime, prezime: osoba.prezime, uloga: osoba.uloga, sala: rezervacija2.Sala.naziv });
            else
                osobe.push({ ime: osoba.ime, prezime: osoba.prezime, uloga: osoba.uloga, sala: "U kancelariji" });
        }
    }

    res.send(JSON.stringify(osobe));


});

app.get("/spisaksala", async function(req, res){
    const sale = await db.Sala.findAll();
    res.setHeader("Content-Type", "application/json");
    res.send(JSON.stringify(sale));
});


app.get("/routes/osoblje.js", function (req, res) {

    res.sendFile('routes/osoblje.js', { root: __dirname });
});
app.post("/rezervacija",async function (req, res) {
    //nisam uradio još!
    let semestar = 0;
    const zimski = [10, 11, 12, 1, 2];
    const ljetni = [3, 4, 5, 6];
    let dan=0;
    const d = req.body.datum.split('.');
    // let datum = new Date(req.body.datum);
    let dateString = `${d[1]}.${d[0]}.${d[2]}`;
    let datum = new Date(dateString);
    const mjesec = datum.getMonth() + 1;
    dan = (datum.getDay() + 6) % 7;
    console.log("MJESEC", mjesec);
    if (zimski.includes(mjesec))
        semestar=1;
    if (ljetni.includes(mjesec))
        semestar=2;
    if(req.body.redovni==false){
        semestar=null;
        dan=null;
    }
    else {
        console.log("SET Datum to NULL!");
        dateString = null;
        // req.body.datum = null;
        console.log(dan, semestar);
    }

    console.log("Insert a new quote: " + JSON.stringify(req.body));
    const result = await db.Termin.create({ ...req.body, semestar, dan, datum: dateString });
    console.log("SUCCESS", JSON.stringify(result));
    const id = result.id;
    const data = { ...req.body, termin: id };
    console.log("DATA:", JSON.stringify(data));
    await db.Rezervacija.create(data);
    res.end();
});
app.get("/routes/pocetna.js", function (req, res) {
    res.sendFile('routes/pocetna.js', { root: __dirname });

});
app.get("/public/javascript/pozivi.js", function (req, res) {
    res.sendFile('/public/javascript/pozivi.js', { root: __dirname });

});
app.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});
module.exports = app;
