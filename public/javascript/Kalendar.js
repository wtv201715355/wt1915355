const nizMjeseci = ['Januar', 'Februar', 'Mart', 'April', 'Maj', 'Juni', 'Juli', 'August', 'Septembar', 'Oktobar', 'Novembar', 'Decembar'];
let Kalendar = (function name(params) {

    var p = [];
    var v = [];

    function presjek(start1, end1, start2, end2) {
        return (start1 > start2 && start1 < end2) || (start2 > start1 && start2 < end1);
    }

    function obojiZauzecaImpl(kalendarRef, mjesec, sala, pocetak, kraj) {
        /* if(mjesec>=0 && mjesec<=11)
        {
            const tbody = kalendarRef.children[0].children[1];
            // const tbody = document.getElementById("kalendar").children[0].children[1];
            const trs = tbody.children;
            // const tds = [].map.call(trs, (trs) => trs.children);
            const tds = [].map.call(trs, (trs) => [].slice.call(trs.children));
            const tdArray = [];
            for(td of tds) {
                for(t of td) {
                    tdArray.push(t);
                }
            }
            // console.log(tdArray);
            for(td of tdArray) {
                if(td.childNodes[0].data == datum) {
                    td.children[0].className = "zauzeta";
                }
            }
            
    
        }
        else{
            throw "neispravan format mjeseca";
        } */

        debugger;
        for (zauzece of v) {
            // mjesec, sala, pocetak, kraj
            // pocetak, kraj - presjek intervala!
            // if(zauzece.datum.split(".")[1] == mjesec && zauzece.naziv == sala, zauzece.pocetak == pocetak && zauzece.kraj == kraj) {
            if (zauzece.datum.split(".")[1] == mjesec && zauzece.naziv == sala, presjek(zauzece.pocetak, zauzece.kraj, pocetak, kraj)) {
                const datum = zauzece.datum.split(".")[0] - 0;
                const tbody = kalendarRef.children[0].children[1];
                // const tbody = document.getElementById("kalendar").children[0].children[1];
                const trs = tbody.children;
                // const tds = [].map.call(trs, (trs) => trs.children);
                const tds = [].map.call(trs, (trs) => [].slice.call(trs.children));
                const tdArray = [];
                for (td of tds) {
                    for (t of td) {
                        tdArray.push(t);
                    }
                }
                // console.log(tdArray);
                for (td of tdArray) {
                    if (td.childNodes[0] && td.childNodes[0].data == datum) {
                        td.children[0].className = "zauzeta";
                    }
                }
            }
        }

        for (zauzece of p) {
            let mjeseci = [];
            if (zauzece.semestar === "zimski")
                mjeseci = [10, 11, 12, 1];
            else if (zauzece.semestar === "ljetni")
                mjeseci = [2, 3, 4, 5, 6];

            if (mjeseci.includes(mjesec) && zauzece.naziv == sala, presjek(zauzece.pocetak, zauzece.kraj, pocetak, kraj)) {
                const datumi = [];
                // staviti ovdje broj dana...
                for (let i = 1; i <= 31; i++) {
                    const d = new Date(`${mjesec + 1}.${i}.2019`);
                    if (d.getDay() == zauzece.dan) {
                        datumi.push(i);
                        // obojiZauzeca(document.getElementById("kalendar"), mjesec, zauzece.naziv, zauzece.pocetak, zauzece.kraj, i);


                        const datum = i;
                        const tbody = kalendarRef.children[0].children[1];
                        // const tbody = document.getElementById("kalendar").children[0].children[1];
                        const trs = tbody.children;
                        // const tds = [].map.call(trs, (trs) => trs.children);
                        const tds = [].map.call(trs, (trs) => [].slice.call(trs.children));
                        const tdArray = [];
                        for (td of tds) {
                            for (t of td) {
                                tdArray.push(t);
                            }
                        }
                        // console.log(tdArray);
                        for (td of tdArray) {
                            if (td.childNodes[0] && td.childNodes[0].data == datum) {
                                td.children[0].className = "zauzeta";
                            }
                        }



                    }
                }
            }
        }

    }


    function ucitajPodatkeImpl(periodicna, vanredna) {
        /* periodicna.forEach(zauzece => {
            let mjeseci = [];
            if(zauzece.semestar === "zimski")
                mjeseci = [9, 10, 11, 0];
            else if(zauzece.semestar === "ljetni")
                mjeseci = [1, 2, 3, 4, 5];
            for(mjesec of mjeseci) {
                // obojiZauzeca(document.getElementById("kalendar"), mjesec, zauzece.sala, zauzece.pocetak, zauzece.kraj);
                const datumi = [];
                for(let i = 1; i <= 31; i++) {
                    const d = new Date(`${mjesec + 1}.${i}.2019`);
                    if(d.getDay() == zauzece.dan) {
                        datumi.push(i);
                        obojiZauzeca(document.getElementById("kalendar"), mjesec, zauzece.naziv, zauzece.pocetak, zauzece.kraj, i);
                    }
                }
            }
        });
        vanredna.forEach(zauzece => {
            obojiZauzeca(document.getElementById("kalendar"), zauzece.datum.split(".")[1], zauzece.naziv, zauzece.pocetak, zauzece.kraj);    
        }); */
        p = periodicna;
        v = vanredna;
    }
    function iscrtajKalendarImpl(kalendarRef, mjesec) {
        // uvecati mjesec za 1 na svim mjetima ?
        mjesec = mjesec + 1;

        kalendarRef.innerHTML = "";
        const datum = new Date(`${mjesec}.1.2019`);
        //let today = new Date();
        let lastDayOfMonth = new Date(datum.getFullYear(), datum.getMonth() + 1, 0);
        console.log(lastDayOfMonth)
        const brojDana = lastDayOfMonth.getDate();//hardkodirani broj dana
        const prviDanUmjesecu = datum.getDay();//pon, uto, sri...
        console.log("prvi dan u mjesecu: ", prviDanUmjesecu);
        console.log("Broj dana: : ", brojDana);
        let nazivMj = document.getElementById("nazivMjeseca");
        //const nizMjeseci = ['Januar', 'Februar', 'Mart', 'April', 'Maj', 'Juni', 'Juli', 'August', 'Septembar', 'Oktobar', 'Novembar', 'Decembar'];
        nazivMj.innerHTML = nizMjeseci[mjesec - 1];

        const tabela = document.createElement("table");
        const header = document.createElement("thead");
        let tr = document.createElement("tr");

        let th = document.createElement("th");
        th.innerHTML = "PON";
        tr.appendChild(th);
        th = document.createElement("th");
        th.innerHTML = "UTO";
        tr.appendChild(th);
        th = document.createElement("th");
        th.innerHTML = "SRI";
        tr.appendChild(th);
        th = document.createElement("th");
        th.innerHTML = "ČET";
        tr.appendChild(th);
        th = document.createElement("th");
        th.innerHTML = "PET";
        tr.appendChild(th);
        th = document.createElement("th");
        th.innerHTML = "SUB";
        tr.appendChild(th);
        th = document.createElement("th");
        th.innerHTML = "NED";
        tr.appendChild(th);

        header.appendChild(tr);
        tabela.appendChild(header);

        tr = document.createElement("tr");
        const tb = document.createElement("tbody");

        const offset = (prviDanUmjesecu + 6) % 7;
        console.log("offset: ", offset);

        for (let i = 0; i < brojDana + offset; i++) {
            /* if((i - offset) % 7 === 0){
                tr=document.createElement("tr");//kreiramo novi red
 
            } */

            if (i === 0)
                for (let j = 0; j < offset; j++) {
                    td = document.createElement("td");
                    td.className = "nevidljiveCelije"
                    tr.appendChild(td);
                    i++;
                }

            td = document.createElement("td");
            td.innerHTML = i + 1 - offset;
            let div = document.createElement("div");
            div.className = "slobodna";
            div.innerHTML = "&nbsp;";
            td.appendChild(div);
            tr.appendChild(td);

            // debugger;
            /* if((i - offset) % 7 === 6){
                tb.appendChild(tr);
            } */
            if (tr.childNodes.length === 7) {
                tb.appendChild(tr);
                tr = document.createElement("tr");
            }

        }

        if (tr.childNodes.length > 0)
            tb.appendChild(tr);

        tabela.appendChild(tb);
        kalendarRef.appendChild(tabela);

        Array.from(document.querySelectorAll("td"))
            .forEach(td =>
                td.addEventListener("click", () => {
                    // console.log(td.childNodes[0].textContent)
                    if (td.childNodes[0]) {
                        if (confirm("Jeste li sigurni da zelite rezervisati salu?")) {

                            const data = {
                                sala: document.getElementById("sala").value,
                                pocetak: document.getElementById("pocetak").value,
                                kraj: document.getElementById("kraj").value,
                                datum: `${parseInt(td.childNodes[0].textContent) < 10 ? '0' : ''}${td.childNodes[0].textContent}.${nizMjeseci.indexOf(document.getElementById("nazivMjeseca").innerHTML) + 1}.2019`,
                                osoba: document.getElementById("osoba").value,
                                redovni:document.getElementById("periodicnost").checked
                            };
                            Pozivi.slanjePodatakaRezervacija(data);



                        }
                    }
                })
            );
    }

    return {
        obojiZauzeca: obojiZauzecaImpl,
        ucitajPodatke: ucitajPodatkeImpl,
        iscrtajKalendar: iscrtajKalendarImpl
    }
}());

// declare module;
//module.exports=Kalendar;