let Pozivi = (function name(params) {
    function slanjePodatakaRezervacijaImpl(data) {
        fetch("/rezervacija", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(() => {
            alert("Uspjesno ste izvrsili rezervaciju!");
        }).catch((error) => {
            alert(error);
        });
    }
    async function citanjeSlikaImp(page) {
        let img1;
        let img2;
        let img3;

        if (!data.images[page * 3 + 1])
            img1 = await fetch(`img\\${page * 3 + 1}.jpg`);

        if (!data.images[page * 3 + 2] && page < 3)
            img2 = await fetch(`img\\${page * 3 + 2}.jpg`);

        if (!data.images[page * 3 + 3] && page < 3)
            img3 = await fetch(`img\\${page * 3 + 3}.jpg`);
        return { img1, img2, img3 };

    }
    function citanjePodatakaImp() {
        return fetch('/podaci').then(response => response.json());
    }
    function posjeteImp(params) {
        return fetch("/posjete").then(response => response.json());
        
    }
    function osobljeImp() {
        return fetch('/osoblje').then(response => response.json());
    }
    function rezervacijeImp() {
        return fetch('/podaci').then(response => response.json());//da bure ruta /rezervacije ako budem htio da dodam u index.js
    }
    function zauzetoOsobljeImp() {
        return fetch('/zauzetoosoblje').then(response => response.json());
    }
    function spisakSalaImp() {
        return fetch('/spisaksala').then(response => response.json());
    }


    return {
        slanjePodatakaRezervacija: slanjePodatakaRezervacijaImpl,
        citanjeSlika: citanjeSlikaImp,
        citanjePodataka: citanjePodatakaImp,
        posjete:posjeteImp,
        osoblje:osobljeImp,
        rezervacije:rezervacijeImp,
        osobljeSale:zauzetoOsobljeImp,
        spisakSala:spisakSalaImp
    }


}());

// module.exports=Pozivi;
// export default Pozivi;