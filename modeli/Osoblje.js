const Sequelize = require("sequelize");

module.exports = function (sequelize, DataTypes) {
    const Osoblje = sequelize.define('Osoblje', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
            //vjestacki primarni kljuc, zato sto nije moguce kreirati tabelu bez PKa
        },
        ime: {
            type: Sequelize.STRING
        },
        prezime: {
            type: Sequelize.STRING

        },
        uloga: {
            type: Sequelize.STRING
        }
    }, { freezeTableName: true });

    return Osoblje;

}