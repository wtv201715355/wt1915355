const Sequelize = require("sequelize");


module.exports = function (sequelize, DataTypes) {
    const Sala = sequelize.define('Sala', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
            //vjestacki primarni kljuc, zato sto nije moguce kreirati tabelu bez PKa
        },
        naziv: {
            type: Sequelize.STRING
        }
    }, { freezeTableName: true });
    return Sala;
}
