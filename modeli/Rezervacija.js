const Sequelize = require("sequelize");

//Rezervacija.hasMany(Termin, {as: 'Termin', foreignKey: 'id'}); 
module.exports = function (sequelize, DataTypes) {
    const Rezervacija = sequelize.define('Rezervacija', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
            //vjestacki primarni kljuc, zato sto nije moguce kreirati tabelu bez PKa
        }
    }, { freezeTableName: true });

    return Rezervacija;
}