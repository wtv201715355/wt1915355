const Sequelize = require("sequelize");


module.exports = function (sequelize, DataTypes) {

    const Termin = sequelize.define('Termin', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
            //vjestacki primarni kljuc, zato sto nije moguce kreirati tabelu bez PKa
        },
        redovni: {
            type: Sequelize.BOOLEAN
            //allowNull: false
        },
        dan: {
            type: Sequelize.INTEGER
            //allowNull: false

        },
        datum: {
            type: Sequelize.STRING
            //allowNull: false
        },
        semestar: {
            type: Sequelize.INTEGER

        },
        pocetak: {
            type: Sequelize.TIME
            //allowNull: false
        },
        kraj: {
            type: Sequelize.TIME
        }
    }, { freezeTableName: true });
    return Termin;
}